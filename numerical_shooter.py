"""
    numerical_shooter_solver()
    
    A function that uses numerical shooting to find limit cycles of a specified ODE.
    
    Parameters
    ----------
    to_optimise : array
    An array containing all the values the user wishes to optimise. The first value of this array is the time period. The remaining values are the initial conditions of the ODE (which will be x0, y0, z0, etc depending on the user's code).
    For example:
        1st initial condition = T (non negitiable, this is the time period.)
        2nd initial condition = 0.4
        3rd initial condition = 2
        4th initial condition = -1
        ... and so on.
    
    ode : function(~s)
    The ODE (or ODEs) to apply shooting to. For each value of t, fun takes an array of [deriv_x , deriv_y , ... ] as per the functions the user decides.
    
    phaseconds : non-negative real value
    The phase condition for the first inputted function. This determines the starting value of the ODE.
    
    parameters : array
    An array of the parameters requried by the ODEs in order to run.
        Example usage:
        For
        --- deriv_x = beta*x - y + delta*x*(x**2 + y**2)
        --- deriv_y = x + beta*y + delta*y*(x**2 + y**2)
        --- deriv_z = -z
        the parameters section will requires 2 parameter values as inputs to run [beta, delta]. If all the required values are not given or if there are too many values, this will produce a valueError.
    
    
    Returns
    -------
    Returns an array containing the corrected initial values for the limit cycle.
    
    Returns -1 as a special value for the period repreesnting non-convergence. This is in the case that the period is zero or negative.
    
    
    Optimal example for input
    -------------------------
        params = [1,-1]
        to_optimise = [20, 0.5, 0.5, 0.5]
        phase_cs = 0.4
        def derivative(X, t, parameters):
            x, y, z = X
            beta, delta = parameters
            deriv_x = beta*x - y + delta*x*(x**2 + y**2)
            deriv_y = x + beta*y + delta*y*(x**2 + y**2)
            deriv_z = -z
            U = [deriv_x, deriv_y, deriv_z]
            return U
    """
#--------------------------------------------------
#--------------------------------------------------
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import scipy.optimize as opt
import sys
#--------------------------------------------------
#--------------------------------------------------

def integrator(to_optimise, fun, phase_cs, parameters):
    t = np.linspace(0, to_optimise[0], 1000)
    initialConds = to_optimise[1:]
    try:
        sol = odeint(fun, initialConds, t, args=(parameters,))
    except ValueError:
        print('\nA value error has been detected.\n')
        print(' -- Ensure the number of input parameters you wish to optimise matches the number of required parameters by the ODEs.\n')
        sys.exit(1)
    except TypeError:
        print('\nA type error has been deteced.\n')
        print(' -- Ensure the types of inputs match those required by the numerical function.\n')
        sys.exit(1)
    except:
        print('\nUnexpected error:',sys.exc_info,'\n')
        sys.exit(1)
    last_vals = sol[-1,:]
    error_T = to_optimise[1] - phase_cs
    otherErrors = last_vals - to_optimise[1:]
    allErrors = np.append(error_T, otherErrors)
    return allErrors


def numerical_shooter_solver(to_optimise, ode, phaseconds, parameters):
    optimised_values = opt.fsolve(integrator, to_optimise, args = (ode, phaseconds, parameters))
    t = np.linspace(0, 3*optimised_values[0], 1000)
    
    to_optimise[0] = optimised_values[0]
    optimised_values_2 = opt.fsolve(integrator, to_optimise, args = (ode, phaseconds, parameters))
    
    sol = odeint(ode, optimised_values[1:], t, args=(parameters,))
    
    if optimised_values[0] < (10**(-8)):
        optimised_values[0] = -1
        return optimised_values
    
    initialConds = to_optimise[1:]
    t   = np.linspace(0,    to_optimise[0], 1000)
    t_2 = np.linspace(0,  2*to_optimise[0], 1000)
    integ_1 = odeint(ode, initialConds, t, args=(parameters,))
    integ_2 = odeint(ode, initialConds, t_2, args=(parameters,))
    integ_compare = abs(integ_1[-1,:] - integ_2[-1,:])
    
    if integ_compare[0] > 10**(-4):
        optimised_values[0] = -1
        return optimised_values

    return optimised_values







