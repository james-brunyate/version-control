"""
    parameter_cont()
    
    A function that takes the first item in the array of parameters given for the ODEs given and finds the value for this parameter that gives the smallest time period value.
    
    Parameters
    ----------
    to_optimise : array
    An array containing all the values the user wishes to optimise. The first value of this array is the time period. The remaining values are the initial conditions of the ODE (which will be x0, y0, z0, etc depending on the user's code).
    For example:
    1st initial condition = T (non negitiable, this is the time period.)
    2nd initial condition = 0.4
    3rd initial condition = 2
    4th initial condition = -1
    ... and so on.
    
    ode : function(~s)
    The ODE (or ODEs) to apply shooting to. For each value of t, fun takes an array of [deriv_x , deriv_y , ... ] as per the functions the user decides.
    
    phaseconds : non-negative real value
    The phase condition for the first inputted function. This determines the starting value of the ODE.
    
    parameters : array
    An array of the parameters requried by the ODEs in order to run.
    Example usage:
    For
    --- deriv_x = beta*x - y + delta*x*(x**2 + y**2)
    --- deriv_y = x + beta*y + delta*y*(x**2 + y**2)
    --- deriv_z = -z
    the parameters section will requires 2 parameter values as inputs to run [beta, delta]. If all the required values are not given or if there are too many values, this will produce a valueError.
    
    lower_lim : value
    The lower limit for a testing different values of a parameter.
    Example:
        1
    
    upper_lim : value
    The upper limit for a testing different values of a parameter.
    Example:
        2
    
    Returns
    -------
    Returns an array of size 1000 rows by 2 columns.
    The first column contains parameter variation from the lower limit to the upper limit.
    The second column contains the time period (the values extracted from the numerical_shooter_solver function).
    Obtained values for the time period that are non-convergent will be represented by a unique value of -1 from numerical_shooter_solver and hence returned to parameter_cont in the array as -1 next to the corresponding parameter value.
    """
#--------------------------------------------------
#--------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from numerical_shooter import numerical_shooter_solver
#--------------------------------------------------
#--------------------------------------------------

def parameter_cont(to_optimise, ode, phaseconds, parameters, lower_lim, upper_lim):
    n_steps = 1000
    p_range = np.linspace(lower_lim, upper_lim, n_steps)
    para_sol = []
    
    for i in p_range:
        parameters[0] = i
        numerical_output = numerical_shooter_solver(to_optimise, ode, phaseconds, parameters)
        para_sol.append(numerical_output[0])

        if numerical_output[0] > 10**(-8):
            to_optimise = numerical_output

    output_array = np.zeros((n_steps,2))

    for i in range(n_steps):
        output_array[i,0] = p_range[i]
        output_array[i,1] = para_sol[i]

    return output_array
